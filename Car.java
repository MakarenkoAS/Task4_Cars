package ua.org.oa.makarenkoas;

import java.util.ArrayList;

public abstract class Car {
	static ArrayList<Car> cars = new ArrayList<>();
	private String name;
	private String fuelType;
	private int numberOfWheels;
	private int horsePower;
	private double targetPrice;
	private double targetTonnageStart;
	private double targetTonnageEnd;
	private double targetSpeed;
	
	// Getters and setters start
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	
	public String getFuelType() {
		return fuelType;
	}
	public void setFuelType(String fuelType) {
		this.fuelType = fuelType;
	}
	
	public int getNumberOfWheels() {
		return numberOfWheels;
	}
	public void setNumberOfWheels(int numberOfWheels) {
		this.numberOfWheels = numberOfWheels;
	}
	
	public int getHorsePower() {
		return horsePower;
	}
	public void setHorsePower(int horsePower) {
		this.horsePower = horsePower;
	}
	
	public double getTargetPrice() {
		return targetPrice;
	}
	public void setTargetPrice(double targetPrice) {
		this.targetPrice = targetPrice;
	}
	
	public double getTargetTonnageStart() {
		return targetTonnageStart;
	}
	public void setTargetTonnageStart(double targetTonnageStart) {
		this.targetTonnageStart = targetTonnageStart;
	}
	public double getTargetTonnageEnd() {
		return targetTonnageEnd;
	}
	public void setTargetTonnageEnd(double targetTonnageEnd) {
		this.targetTonnageEnd = targetTonnageEnd;
	}
	public double getTargetSpeed() {
		return targetSpeed;
	}
	public void setTargetSpeed(double targetSpeed) {
		this.targetSpeed = targetSpeed;
	}
	// Getters and setters end
	
	// Car constructor
	public Car(String name, String fuelType, int numberOfWheels, int horsePower) {
		this.name = name;
		this.fuelType = fuelType;
		this.numberOfWheels = numberOfWheels;
		this.horsePower = horsePower;
	}
	
	// Method to add cars in list
	public static void addCar(Car addingCar) {
		cars.add(addingCar);
	}
	
	// Method to compare passenger cars to target price
	public static void comparePrice(double targetPrice) {

		System.out.println("Target price is: " + targetPrice);
		for (int i = 0; i < cars.size(); i++) {
			if (cars.get(i).getTargetPrice() < targetPrice) {
				System.out.println(cars.get(i).getName() + " price is " + cars.get(i).getTargetPrice()
						+ ", it's less then target price;");
			}
		}
	}

	// Method to search trucks in target tonnage range
	public static void compareTonnage(double targetTonnageStart, double targetTonnageEnd) {

		System.out.println("Target tonnage range of trucks is: from " + targetTonnageStart + " to " + targetTonnageEnd + " tonns");
		for (int i = 0; i < cars.size(); i++) {
			if (cars.get(i).getTargetTonnageStart() > targetTonnageStart&&cars.get(i).getTargetTonnageEnd() < targetTonnageEnd) {
				System.out.println(cars.get(i).getName() + " tonnage is " + cars.get(i).getTargetTonnageStart() + " tonns"
						+ ", it fits the range;");
			}
		}
	}
	
	// Method to compare racing cars to target speed
	public static void compareSpeed(double targetSpeed) {

		System.out.println("Target speed of racing cars is: "  + targetSpeed + " km/h");
		for (int i = 0; i < cars.size(); i++) {
			if (cars.get(i).getTargetSpeed() > targetSpeed) {
				System.out.println(cars.get(i).getName() + " speed is: " + cars.get(i).getTargetSpeed() + " km/h"
						+ " , it's more then target speed;");
			}
		}
	}

	@Override
	public String toString() {
		return getClass().getSimpleName() + " [name=" + name + ", fuelType=" + fuelType
				+ ", numberOfWheels=" + numberOfWheels + ", horsePower=" + horsePower + "]";

	}

	
}
