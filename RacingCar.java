package ua.org.oa.makarenkoas;

public class RacingCar extends Car {


	public RacingCar(String name, String fuelType, int numberOfWheels, int horsePower, double speed) {
		super(name, fuelType, numberOfWheels, horsePower);
		super.setTargetSpeed(speed);

	}
	@Override
	public String toString() {
		return getClass().getSimpleName() + " [name=" + getName() + ", fuelType=" + getFuelType()
				+ ", numberOfWheels=" + getNumberOfWheels() + ", horsePower="
				+ getHorsePower() + ", speed=" + getTargetSpeed() + "]" + "\n";

	}
}
