package ua.org.oa.makarenkoas;

public class PassengerCar extends Car {
	

	public PassengerCar(String name, String fuelType, int numberOfWheels, int horsePower, double price) {
		super(name, fuelType, numberOfWheels, horsePower);
		super.setTargetPrice(price);
	}
	
	public void showInfo(){
		System.out.println(getClass().getSimpleName() + getName() + getNumberOfWheels() + getHorsePower()
		+ getTargetPrice());
	}
	
	@Override
	public String toString() {
		return getClass().getSimpleName() + " [name=" + getName() + ", fuelType=" + getFuelType()
				+ ", numberOfWheels=" + getNumberOfWheels() + ", horsePower="
				+ getHorsePower() + ", price=" + getTargetPrice() + "]"  + "\n";

	}
	
}
