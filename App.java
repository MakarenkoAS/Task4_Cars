package ua.org.oa.makarenkoas;

/*
 * @version 1.0 12 Mar 2017 
 * @author Makarenko Alexander
 */

public class App {

	public static void main(String[] args) {
		Car mosckvich = new PassengerCar("Moskvich", "Gas", 4, 160, 2000);
		Car renault = new PassengerCar("Renault", "Gasoline", 4, 240, 18000);
		Car lada = new PassengerCar("Lada", "Gas", 4, 180, 3000);
		Car zaporojets = new PassengerCar("Zaporojets", "Gas", 4, 60, 1000);
		Car ford = new PassengerCar("Ford", "Gasoline", 4, 260, 15000);
		
		Car.addCar(mosckvich);
		Car.addCar(renault);
		Car.addCar(lada);
		Car.addCar(zaporojets);
		Car.addCar(ford);
		
		Car.comparePrice(18_500);
		System.out.println("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");
		Car porsche = new RacingCar("Porsche", "Gasoline", 4, 500, 320);
		Car mclaren = new RacingCar("McLaren", "Gasoline", 4, 800, 350);
		Car bugatti = new RacingCar("Bugatti", "Gasoline", 4, 1000, 400);
		Car lamborghini = new RacingCar("Lamborghini", "Gasoline", 4, 560, 330);
		Car ferrari = new RacingCar("Ferrari", "Gasoline", 4, 750, 360);
		
		Car.addCar(porsche);
		Car.addCar(mclaren);
		Car.addCar(bugatti);
		Car.addCar(lamborghini);
		Car.addCar(ferrari);
		
		Car.compareSpeed(320);
		System.out.println("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");
		Car kamaz = new Truck("KamAZ", "Diesel", 10, 400, 12);
		Car maz = new Truck("MAZ", "Diesel", 10, 380, 10);
		Car man = new Truck("MAN", "Diesel", 10, 460, 16);
		Car daf = new Truck("DAF", "Diesel", 10, 500, 20);
		Car kraz = new Truck("KrAZ", "Diesel", 10, 360, 8);
		
		Car.addCar(kamaz);
		Car.addCar(maz);
		Car.addCar(man);
		Car.addCar(daf);
		Car.addCar(kraz);
		
		Car.compareTonnage(11, 18);
		System.out.println("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");
		System.out.println(Car.cars);
	}

}
