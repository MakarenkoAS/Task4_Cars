package ua.org.oa.makarenkoas;


public class Truck extends Car {

	public Truck(String name, String fuelType, int numberOfWheels, int horsePower, double tonnage) {
		super(name, fuelType, numberOfWheels, horsePower);
		super.setTargetTonnageStart(tonnage);

	}
	@Override
	public String toString() {
		return getClass().getSimpleName() + " [name=" + getName() + ", fuelType=" + getFuelType()
				+ ", numberOfWheels=" + getNumberOfWheels() + ", horsePower="
				+ getHorsePower() + ", tonnage=" + getTargetTonnageStart() + "]"  + "\n";

	}
}
